<?php

# AgroAnalytics          	          	          	    
# Copyright (C) 2010 by BIGSHOT  	   	   	   	   	   	   	   	   	 
# Homepage   : www.jonathanleek.com		   	   	   	   	   	   		 
# Author     : Jonathan Leek	    		   	   	   	   	   	   	   	 
# Email      : me@jonathanleek.com 	   	   	   	   	   	   	     
# Version    : 1.0                        	   	    	   	   		 
# License    : http://www.gnu.org/copyleft/gpl.html GNU/GPL          
######################################################################

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.plugin.plugin');
jimport('joomla.html.parameter');


class plgSystemAgroanalytics extends JPlugin
{
	function plgSystemAgroanalytics(&$subject, $config)
	{
		parent::__construct($subject, $config);
		$this->_plugin = JPluginHelper::getPlugin( 'system', 'agroanalytics' );
		$this->_params = new JParameter( $this->_plugin->params );
	}
	
	function onAfterRender()
	{
		$mainframe = &JFactory::getApplication();
		
		$web_property_id = $this->params->get('web_property_id', '');

		if ($web_property_id === '' || $mainframe->isAdmin() || strpos($_SERVER["PHP_SELF"], "index.php") === false)
		{
			return;
		}
		$mainframe->enqueueMessage($web_property_id);
		$buffer = JResponse::getBody();
		
		$trackers;
		
		if ((int)$this->params->get('google_analytics') === 1)
		{
			$google_analytics = "
				<script type='text/javascript'>
					var _gaq = _gaq || [];
					_gaq.push(['_setAccount', '" . $web_property_id . "']);
					_gaq.push(['_trackPageview']);
					(function() {
						var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
						ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					})();
				</script>";
				
			$trackers .= $google_analytics;
		}

		if ((int)$this->params->get('agro_analytics') === 1) 
		{
			$agro_analytics = '
				<!-- AgroAnalytics -->
				<script type="text/javascript">
				  var _paq = _paq || [];
				  _paq.push([\'trackPageView\']);
				  _paq.push([\'enableLinkTracking\']);
				  (function() {
				    var u="//' . $this->params->get('agro_path') . '/";
				    _paq.push([\'setTrackerUrl\', u+\'piwik.php\']);
				    _paq.push([\'setSiteId\', ' . $this->params->get('agro_site_id') . ']);
				    var d=document, g=d.createElement(\'script\'), s=d.getElementsByTagName(\'script\')[0];
				    g.type=\'text/javascript\'; g.async=true; g.defer=true; g.src=u+\'piwik.js\'; s.parentNode.insertBefore(g,s);
				  })();
				</script>
				<noscript><p><img src="//' . $this->params->get('agro_path') . '/piwik.php?idsite=' . $this->params->get('agro_site_id') . '" style="border:0;" alt="" /></p></noscript>
				<!-- End AgroAnalytics Tracking Code -->';
			
			$trackers .= $agro_analytics;
		}
		
		$buffer = str_replace("</body>", $trackers."</body>", $buffer);
		JResponse::setBody($buffer);
		return true;
	}
}
